# openwino.cc #

Community platform for the OpenWino project.

During development, the database will be hosted on a personal server.

### Installation

Checkout the project in the root folder of any php able webserver. The urls to access the website must be of the form : localhost:8000/openwinocc/*. This is because of the shared database which stores url prefix information. A migration will have to be performed on the production version.

### Follow development 
[On Trello](https://trello.com/b/jk9jxAWY/openwino-cc-v0)