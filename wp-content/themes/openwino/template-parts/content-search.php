<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openwino
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class();?>>
	<?php set_post_thumbnail_size( 120, 120);
	the_post_thumbnail(); ?>
	<div class="article-content">
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

			<div class="entry-meta">
				<?php openwino_posted_on(); ?>
			</div><!-- .entry-meta -->
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

		<!-- <footer class="entry-footer">
			<?php //openwino_entry_footer(); ?>
		</footer> 
		-->
		<!-- .entry-footer (Isn't really necessary) -->
	</div>
</article><!-- #post-## -->
