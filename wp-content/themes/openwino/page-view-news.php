<?php
/**
 * Template for the new page.
 * 
 * @package openwino
 */

get_header(); ?>

	<div id="primary" class="content-area center-wrap">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Only search for news posts. */
			$args = array(
				'post_type' => 'news',
				'orderby' => 'date',
				'order' => 'desc',
				'posts_per_page' => '10',
				'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 ),
			);
			$wp_query = new WP_Query($args);

			/* Start the Loop */
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', get_post_format() );
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>