<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package openwino
 */

get_header(); ?>

	<section id="primary" class="content-area center-wrap">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
                <form role="search" method="get" class="search-form-inner" action="">
					<label>
						<span class="screen-reader-text">Search for:</span>
						<input type="search" class="search-field-inner" placeholder="Search tutorials ..." value="" name="s" title="Search for:">
					</label>
					<button type="submit" class="searchButtonInner">
        				<i class="fa fa-search fa-lg"></i>
    				</button>
    			</form>
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'openwino' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
