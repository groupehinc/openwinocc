<?php
/**
 * Template for the tutorial page.
 *
 * @package openwino
 */

get_header(); ?>

	<div id="primary" class="content-area center-wrap">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			?>
			<div class="filter_wrap">
				<a href="?orderby=date">Order by date</a>
				<a href="?orderby=meta_value_num">Order by likes</a>
			</div>
			<?php

			/* Only search for tutorial posts. */
			$orderby = (isset($_GET['orderby'])) ? $_GET['orderby'] : 'date';
			$args = array(
				'post_type' => 'tutorials',
				'meta_key' => '_liked',
				'orderby' => $orderby,
				'order' => 'desc',
				'posts_per_page' => '10',
				'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 ),
			);
			if($orderby == 'date') {
				unset($args['meta_key']);
			}
			$wp_query = new WP_Query($args);

			/* Start the Loop */
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				// get_template_part( 'template-parts/content', get_post_format() );
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();
			wp_reset_postdata();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->


	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
