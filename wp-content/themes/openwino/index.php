<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package openwino
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main center-wrap" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Search for news posts. */
			$args = array(
				'post_type' => 'news',
				'post_status' => 'publish',
				'orderby' => 'date',
				'order' => 'desc',
				'posts_per_page' => '4',
			);
			$wp_query = new WP_Query($args);

			?>
            <h2 class="categories-title">News</h2>
			<div class="articles-flex-wrap">

			<?php
			/* Start the Loop */
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search');

			endwhile;?>

			</div>

			<?php
			wp_reset_postdata();

			echo '<hr>';

			/* Search for tutorials posts with most ratings and in the last 30 days. */
			$args = array(
				'post_type' => 'tutorials',
				'post_status' => 'publish',
				'meta_key' => '_liked',
				'orderby' => 'meta_value_num',
				'order' => 'desc',
				'posts_per_page' => '10',
				'date_query' => array(
					'column' => 'post_date',
					'after' => '- 30 days'
				),
			);
			$wp_query = new WP_Query($args);

			if($wp_query->post_count <= 4) {
				unset($args['date_query']);
				$wp_query = new WP_Query($args);
			}

			?>
            <h2 class="categories-title">Tutorials</h2>
			<div class="articles-flex-wrap">

			<?php
			/* Start the Loop */
			while ( $wp_query->have_posts() ) : $wp_query->the_post();


				/* Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				*/
				get_template_part( 'template-parts/content', 'search');

			endwhile;?>

			</div>

			<?php
			wp_reset_postdata();

			// if (function_exists('get_highest_rated_category')):
			// 	echo '<ul>';
			// 		get_highest_rated_category(get_cat_ID('tutorials'));
			// 	echo '</ul>';
			// endif;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
